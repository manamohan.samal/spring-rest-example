package com.accenture.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accenture.entity.UserEntity;

@Repository
public interface UserDao extends JpaRepository<UserEntity, Long>{

}
