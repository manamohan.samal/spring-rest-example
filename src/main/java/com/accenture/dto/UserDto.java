package com.accenture.dto;

import com.accenture.entity.Address;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
	private String userId;
	private String userName;
	private String password;
	private Address address;
	private String email;
	private String phone;

}
