package com.accenture.service;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.accenture.dto.UserDto;
import com.accenture.entity.UserEntity;

public interface UserService {

	public UserDto addUser(UserDto userDto);
	
	public List<UserDto> getUserList();
	
	public static UserEntity convertToEntity(UserDto userDto) {
		UserEntity userEntity = new UserEntity();
		
		BeanUtils.copyProperties(userDto, userEntity);
		return userEntity;
	}
	
	public static UserDto convertToDto(UserEntity userEntity) {
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userEntity, userDto);
		return userDto;
	}
}
