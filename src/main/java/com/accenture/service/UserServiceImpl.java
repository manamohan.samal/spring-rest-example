package com.accenture.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.dao.UserDao;
import com.accenture.dto.UserDto;
import com.accenture.entity.UserEntity;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao repository;

	@Override
	public UserDto addUser(UserDto userDto) {
		return UserService.convertToDto(repository.save(UserService.convertToEntity(userDto)));
	}

	@Override
	public List<UserDto> getUserList() {
		List<UserEntity> entityList = repository.findAll();
		
		return entityList.stream()
				.map(UserService :: convertToDto)
				.collect(Collectors.toList());
	}

}
