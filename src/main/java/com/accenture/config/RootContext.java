package com.accenture.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = {"com.accenture.service"})
@Import(DBConfig.class)
public class RootContext {

}
