package com.accenture.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppConfig extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		System.out.println("AppConfig.getRootConfigClasses()");
		return new Class[] {RootContext.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		System.out.println("AppConfig.getServletConfigClasses()");
		return new Class[] {ServletContext.class};
	}

	@Override
	protected String[] getServletMappings() {
		System.out.println("AppConfig.getServletMappings()");
		return new String[] {"/"};
	}

}
