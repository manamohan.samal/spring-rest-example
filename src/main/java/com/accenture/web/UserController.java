package com.accenture.web;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.dto.ResponseDto;
import com.accenture.dto.UserDto;
import com.accenture.entity.Address;
import com.accenture.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping(value = "/user", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDto> adduser(@RequestBody UserDto userDto) {
		UserDto userDto2 = userService.addUser(userDto);

		ResponseDto responseDto = ResponseDto.builder().message("success").object(userDto2).build();
		return new ResponseEntity<ResponseDto>(responseDto, HttpStatus.OK);
	}

	@GetMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
	public String getUserList() throws JsonProcessingException {
		return new ObjectMapper().writeValueAsString(userService.getUserList());
	}
	
	@GetMapping(value = "/index", produces = MediaType.APPLICATION_JSON_VALUE)
	public String index() throws JsonProcessingException {
		UserDto userDto = UserDto.builder().userName("manamohan").password("password").email("manamohan@gmail.com")
				.phone("1234567890").address(new Address("kjr", "kjr", "odisha", "758022")).build();
		
		List<UserDto> list = Arrays.asList(userDto);
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(list);
	}
}
