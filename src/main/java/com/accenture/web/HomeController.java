package com.accenture.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

//	@RequestMapping(value="/hello", produces = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping("/hello")
	public String welcome() {
		return "Hello World";
	}
	
	@RequestMapping(value="/hi", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getStarted(){
		ResponseEntity<String> response = new ResponseEntity<String>("hii", HttpStatus.ACCEPTED);
		return response;
	}
}